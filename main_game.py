"""
B109784 - Software development assignment -
The War of Artic Game Prototype

== MAIN MENU SECTION ==
Ver 1.0, 30 March 2018

This is the main menu section of the game. This python is
programmed on Windows platform.
This program uses os.system() to clear the screen. Windows
version is os.system('cls'). If the user want to run it on
Linux, please change it into os.system('clear')

The main menu list:
> MULTIPLAYER
> SQUAD SETTING
> TUTORIAL
> OPTIONS
> WATCH
> QUIT GAME

== Def for page screen in this code ==
> def main_menu = main menu screen
> def multiplayer_lobby = multiplayer lobby pages
> def quickplay = multiplayer quickplay page
> def search_server = multiplayer search server page
> def play_with_friend = multiplayer play with friend page
> def play_with_bot = multiplayer play with bot page
> def game_options = game options page

== def for actions ==
> def game_mode_selection = select game mode
> def map_selection = select map
> def graphic_change = dummy prototype to change game graphic
"""

import sys
import os

"""
================= GLOBAL VARIABLE LISTS ======================
These variables are accessed from def functions below in order
update its values
"""
#=============================================================
"""
---------------- Select Game Mode ----------------------------
The default value is the last index of the game mode array 
which is 'Not Selected'. Bunch of multiplayer related pages will
access game_mode_choice variable to update the game mode status.
--------------------------------------------------------------
"""
campaign = ['Hold the flag','Deathmatch','Elite Deathmatch','Not Selected']
len_campaign = len(campaign)
print(len_campaign)
game_mode_choice = len_campaign

"""
---------------- Select Map ----------------------------------
The default value is the last index of the map array which is 
'Not Selected'. Bunch of multiplayer related pages will access 
map_choice variable to update the game mode status.
--------------------------------------------------------------
"""
map_location = ['Semarang','Jogja','Solo','Bandung','Surabaya','Not Selected']
len_map = len(map_location)
map_choice = len_map

"""
---------------- Bot difficulty and Options ------------------
bot_diff is for play_with_bot()
select_option is for graphic_change()
--------------------------------------------------------------
"""
bot_diff = 3
select_option = 0

# ============= DEFS =========================================

def main_menu():
    os.system('cls')
    print('----------------- The War of Artic MAIN MENU ---------------------')
    print("MAIN MENU:\n(1) Multiplayer\n(2) Squad Setting\n(3) Tutorial\n(4) Options\n(5) Watch\n(6) QUIT GAME")
    choice = int(input("Your choice: "))

    if choice == 1:
        multiplayer_lobby()

    elif choice == 2:
        print("This section is tested done separately. To be added later")
        main_menu()

    elif choice == 3:
        print("tutorial mode is not yet available in Alpha version")
        main_menu()

    elif choice == 4:
        game_options()

    elif choice == 5:
        print('Watch mode is not yet available in Alpha version')
        main_menu()

    # Exit command
    elif choice == 6:
        print('------------------------------')
        print('are you sure want to quit?\n (1) YES (2) NO')
        print('------------------------------')
        quit_choice = str(input("Select: "))
        if quit_choice == 2:
            sys.exit(0)

    elif choice > 3:
        print('no choice available')


def multiplayer_lobby():
    os.system('cls')
    print('==============================================================')
    print('================== MULTIPLAYER LOBBY =========================')
    print('==============================================================')
    print('Gameplay:\n(1) QUICKPLAY\n(2) SEARCH SERVER\n(3) PLAY WITH FRIEND\n(4) PLAY WITH BOT\n(5) BACK TO MAIN MENU')

    gameplay_choice = int(input('Select: '))

    # Go to quickplay page
    if gameplay_choice == 1:
        quickplay()

    # Go to search server page    
    elif gameplay_choice == 2:
        search_server()

    # Go to play with friend page   
    elif gameplay_choice == 3:
        play_with_friend()

    # Go to play with bot page
    elif gameplay_choice == 4:
        play_with_bot()

    # Back to main menu
    elif gameplay_choice == 5:
        main_menu()

    elif gameplay_choice > 5:
        print('There is no such menu, please retry')
        multiplayer_lobby()


def quickplay():
    global len_campaign
    global len_map
    os.system('cls')
    print('--------------------------------------------------------------')
    print('------------------- MP - QUICKPLAY ---------------------------')
    print('--------------------------------------------------------------')
    print('Fastest way to play with random players')
    print('\nGAME MODE:', campaign[game_mode_choice - 1])
    print('MAP:', map_location[map_choice - 1])
    print('\n(1) SELECT GAME MODE\n(2) SELECT MAP\n(3) GO\n(4) GO BACK')

    quickplay_choice = int(input('Select: '))

    # Go to select game mode
    if quickplay_choice == 1:
        game_mode_selection()
        quickplay()

    # Go to select map
    elif quickplay_choice == 2:
        map_selection()
        quickplay()

    # Play the game!
    elif quickplay_choice == 3:
        # Make sure both of game mode and map are selected
        if (game_mode_choice < len_campaign) and (map_choice < len_map):
            go_play()
        else:
            print('Both of Game Mode and Map must be selected')
            quickplay()

    # Back to multiplayer lobby
    elif quickplay_choice == 4:
        multiplayer_lobby()

    elif quickplay_choice > 4:
        print ('There is no such selection, please retry')
        quickplay()

def search_server():
    os.system('cls')
    print('--------------------------------------------------------------')
    print('------------------- MP - SEARCH SERVER -----------------------')
    print('--------------------------------------------------------------')
    print('Classic way to play online game')
    print('(1) SEARCH SERVER\n(2) GO\n(3) GO BACK')

    searchserver_choice = int(input('Select: '))

    if searchserver_choice == 1:
        print("Sorry, alpha game, no server available yet")
        search_server()

    # Play the game!
    elif searchserver_choice == 2:
        go_play()

    # Back to multiplayer lobby
    elif searchserver_choice == 3:
        multiplayer_lobby()

    elif searchserver_choice > 3:
        print('There is no such menu, please retry')
        search_server()

def play_with_friend():
    global len_campaign
    global len_map
    os.system('cls')
    print('--------------------------------------------------------------')
    print('---------------- MP - PLAY WITH YOUR FRIEND ------------------')
    print('--------------------------------------------------------------')
    print('Play with your friend online!')
    print('\nGAME MODE:', campaign[game_mode_choice - 1])
    print('MAP:', map_location[map_choice - 1])
    print('(1) SELECT GAME MODE\n(2) SELECT MAP\n(3) INVITE FRIEND\n(4) GO\n(5) GO BACK')

    friend_choice = int(input('Select: '))

    # Go to select game mode
    if friend_choice == 1:
        game_mode_selection()
        play_with_friend()

    # Go to select map
    elif friend_choice == 2:
        map_selection()
        play_with_friend()

    elif friend_choice == 3:
        print("Not available yet in alpha testing")
        play_with_friend()

    # Play the game!
    elif friend_choice == 4:
        # Make sure both of game mode and map are selected
        if (game_mode_choice < len_campaign) and (map_choice < len_map):
            go_play()
        else:
            print('Both of Game Mode and Map must be selected')
            play_with_friend()

    # Back to multiplayer lobby
    elif friend_choice == 5:
        multiplayer_lobby()

    elif friend_choice > 4:
        print('There is no such menu, please retry')
        play_with_friend()

def play_with_bot():
    global bot_diff
    global len_campaign
    global len_map
    os.system('cls')
    print('--------------------------------------------------------------')
    print('-------------------- MP - PLAY WITH BOT ----------------------')
    print('--------------------------------------------------------------')
    print('Offline game mode with bot')
    print('\nGAME MODE:', campaign[game_mode_choice - 1])
    print('MAP:', map_location[map_choice - 1])
    print('BOT DIFFICULTY:',bot_diff)
    print('(1) SELECT GAME MODE\n(2) SELECT MAP\n(3) BOT DIFFICULTY\n(4) GO\n(5) GO BACK')

    bot_choice = int(input('Select: '))

    # Go to select game mode
    if bot_choice == 1:
        game_mode_selection()
        play_with_bot()

    # Go to select map
    elif bot_choice == 2:
        map_selection()
        play_with_bot()

    # Bot difficulty setting
    elif bot_choice == 3:
        bot_diff = int(input('Please enter bot difficulty (1-5): '))
        if (bot_diff > 5) or (bot_diff < 1):
            print('Bot difficulty out of range, please retry')
            play_with_bot()
        else:
            play_with_bot()

    # Play the game!
    elif bot_choice == 4:
        # Make sure both of game mode and map are selected
        if (game_mode_choice < len_campaign) and (map_choice < len_map):
            go_play()
        else:
            print('Both of Game Mode and Map must be selected')
            play_with_bot()

    # Back to multiplayer lobby
    elif bot_choice == 5:
        multiplayer_lobby()

    elif bot_choice > 5:
        print('There is no such menu, please retry')
        play_with_bot()

def game_mode_selection():
    global game_mode_choice
    os.system('cls')

    print('==============================================================')
    print("Select game mode: ")
    print("(1) Hold the flag\n(2) Deatchmatch\n(3) Elite Deathmatch")

    game_mode_choice = int(input('Select: '))
    print(game_mode_choice)

    if game_mode_choice > 4:
        print ('There is no such map, please retry')
        game_mode_selection()

    return(game_mode_choice)

def map_selection():
    global map_choice
    os.system('cls')

    print('==============================================================')
    print("Select map: ")
    print("(1) Semarang\n(2) Jogja\n(3) Solo\n(4) Bandung\n(5) Surabaya")
    map_choice = int(input('Select: '))

    if map_choice > 5:
        print ('There is no such map, please retry')
        map_selection()

    return(map_choice)

def go_play():
    os.system('cls')
    war = input('Game WIP :( Please any key to go back to multiplayer lobby')
    if (war != None):
        multiplayer_lobby()

def game_options():
    os.system('cls')
    print('==============================================================')
    print('======================= OPTIONS ==============================')
    print('==============================================================')
    print('(1) Graphics\n(2) Back to main menu')
    option_choice = int(input('Select:'))

    # Go to change graphic page
    if option_choice == 1:
        graphic_change()

    # Back to main menu
    if option_choice == 2:
        main_menu()

    if option_choice > 2:
        print ('There is no such option, please retry')
        game_options()

def graphic_change():
    global select_option

    print('----------------- Graphic Options ---------------------')
    option = ['Low', 'Medium', 'High']
    print('Current: %s' % (option[select_option-1]))
    print('(1) Change graphics\n(2) back')
    graphic_changes = int(input('Select:'))

    # Change the graphic
    if graphic_changes == 1:
        print('------------- Change Graphic Options -----------------')
        print('(1) Low (2) Medium (3) High')
        print('(4) Go Back')
        select_option = int(input('Select:'))
        print('Successfuly changed into %s' % option[select_option-1])
        graphic_change()

    # Back to options page
    if graphic_changes == 2:
        game_options()

    if graphic_changes > 2:
        print ('There is no such option, please retry')
        graphic_change()


# This is a very simple splash screen
print ("================================================")
print ("============== The War of Artic ================")
print ("======== Very Alpha Prototype Version ==========")
print ("================================================")
enter_game = input('Press any key to continue')
if (enter_game != None):
    main_menu()









