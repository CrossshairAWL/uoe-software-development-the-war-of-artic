"""
B109784 - Software development assignment -
The War of Artic Game Prototype

== SQUAD PAGE SECTION ==
Ver 1.0, 30 March 2018

This is the squad setting section of the game. This section
is to set and costumize squad rosters including captain,
hierophant, and soldiers. This python is programmed on Windows 
platform.
This program uses os.system() to clear the screen. Windows
version is os.system('cls'). If the user want to run it on
Linux, please change it into os.system('clear')

== Classes ==
> class soldierList = for soldier stats
> class givenCaptain = for captain stats, items, and skillsets
> class givenHierophant = for hierophant stats, items, and skillsets

== Def for page screen in this code ==
> def squad_lobby = main squad lobby screen for this section
> def soldier_edit = squad roster page
> def captain_edit = to show captain stats. No edit actions yet
in the prototype
> def hierophant_edit = to show hierophant stats. No edit actions
yet in the prototype

== def for actions ==
> def change_squadName = change squad name
> def buy_soldier = buy soldier
"""

import os

class soldierList:
    sol_count = 0

    def __init__(self, index, name, move, fight, shoot, armour, morale, health, cost):
        self.index = index
        self.name = name
        self.move = move
        self.fight = fight
        self.shoot = shoot
        self.armour = armour
        self.morale = morale
        self.health = health
        self.cost = cost
        soldierList.sol_count += 1

    def soldierStat(self):
        print ('Name: ', self.name,'|| COST:',self.cost)
        print ('Move:',self.move,' Fight:',self.fight,' Shoot:',self.shoot,' Armour:',self.armour,' Morale:',self.morale,' Health:',self.health)

class givenCaptain:
    name = 'The Holy Mega Atatatatatata Captain brohh'
    move = 5
    fight = 5
    shoot = 5
    armour = 5
    morale = 5
    health = 10
    XP = 100
    specialism = ['Indomie','Mie Sedap','Mie Ayam']
    skillset = ['Indomie','Mie Ayam']
    items = ['Soto mi','mie kari','gado-gado','Ketoprak']

    def captainStat():
        print('Name: ',captain.name)
        print('Move:',captain.move,' Fight:',captain.fight,' Shoot:',captain.shoot,' Armour:',captain.armour,' Morale:',captain.morale,' Health:',captain.health)
        print('Experience:',captain.XP)
        print('Specialism: ',captain.specialism)
        print('Skillset: ',captain.skillset)
        print('Items: ',captain.items)

class givenHierophant:
    name = 'Hierophant the mega right hand man of the holy captain ah why this name is so long arghh'
    move = 4
    fight = 4
    shoot = 4
    armour = 4
    morale = 4
    health = 8
    XP = 50
    specialism = ['Indomie']
    skillset = ['Indomie']
    items = ['Ketoprak']

    def hierophantStat():
        print('Name: ',hierophant.name)
        print('Move:',captain.move,' Fight:',captain.fight,' Shoot:',captain.shoot,' Armour:',captain.armour,' Morale:',captain.morale,' Health:',captain.health)
        print('Experience:',captain.XP)
        print('Specialism: ',captain.specialism)
        print('Skillset: ',captain.skillset)
        print('Items: ',captain.items)

"""
================= GLOBAL VARIABLE LISTS ======================
These variables are accessed from def functions below in order
update its values
"""
#=============================================================

# List of the available soldiers for now
sol0 = soldierList(1, "Acong", 1, 1, 1, 1, 1, 1, 100)
sol1 = soldierList(2, "Tapir", 2, 2, 2, 2, 2, 2, 200)
sol2 = soldierList(3, "Ofe", 3, 3, 3, 3, 3, 3, 300)
sol3 = soldierList(4, "Zaki", 4, 4, 4, 4, 4, 4, 400)
sol4 = soldierList(5, "Echi", 5, 5, 5, 5, 5, 5, 500)
sol5 = soldierList(6, "Ipeh", 6, 6, 6, 6, 6, 6, 600)

# All of them have to be assigned into this array
sol_list = [sol0, sol1, sol2, sol3, sol4, sol5]
y = len(sol_list)
yy = y

# Total soldiers in the game
sol_count = soldierList.sol_count

# Captain and hierophant
captain = givenCaptain()
hierophant = givenHierophant()

# Array for captain and hierophant
leader_equipped = [captain, hierophant]

# Slot for the soldiers
sol_equipped = []
x = len(sol_equipped)
xx = x

# Player credit. The beginning of the game is 500
credit = 500
# To ensure there's no negative credit
if credit < 0:
    credit = 0

# Experience credit for Captain and Hierophant. 
experience = 100

# Squad name. It can be updated through change_squadName()
squad_name = 'Kelas Bunga Matahari'

# ============= DEFS =========================================

def squad_lobby():
    global squad_name
    os.system('cls')

    print('==============================================================')
    print('====================== SQUAD LOBBY ===========================')
    print('==============================================================')
    print('\n-------------------- CREDIT = %d ---------------------------' % credit)
    print('\n SQUAD NAME: %s' % squad_name)
    print('\nSquad Menu:\n(1) Change Squad Name\n(2) Edit Captain\n(3) Edit Hierophant\n(4) Edit Soldiers\n(5) BACK TO MAIN MENU')
    squad_choice = int(input('Select:'))

    # Go to change squad name
    if squad_choice == 1:
        change_squadName()

    # Go to edit captain
    elif squad_choice == 2:
        captain_edit()

    # Go to edit hierophant
    elif squad_choice == 3:
        hierophant_edit()

    # Go to edit and buy soldiers
    elif squad_choice == 4:
        soldier_edit()

    elif squad_choice == 5:
        print('Not yet linked with main menu')
        squad_lobby()

    elif squad_choice > 5:
        print('There is no such option, please retry')
        squad_lobby()

def change_squadName():
    global squad_name
    os.system('cls')

    print('--------------------------------------------------------------')
    print('-------------------- CHANGE SQUAD NAME -----------------------')
    print('--------------------------------------------------------------')
    print('Current name: %s' % squad_name)
    print('Options:\n(1) Change name\n(2) Back to squad lobby')
    name_choice = int(input('Select:'))

    # Change squad name
    if name_choice == 1:
        change_name = str(input('Enter new name:'))
        squad_name = change_name
        print('Name successfuly changed!')
        change_squadName()

    # Back to squad lobby
    elif name_choice == 2:
        squad_lobby()

    elif name_choice > 2:
        print('There is so such menu, please retry')
        change_squadName()

def soldier_edit():
    global xx
    os.system('cls')

    print('--------------------------------------------------------------')
    print('-------------------- EDIT SQUAD ROSTER -----------------------')
    print('--------------------------------------------------------------')
    print('\n-------------------- CREDIT = %d ----------------------------' % credit)
    print('Soldier equipped = ', xx)
    print('(1) Buy Soldier\n(2) Back to soldier page')
    edit_choice = int(input('Select:'))

    # Go to buy soldier page
    if edit_choice == 1:
        buy_soldier()

    # Back to squad lobby
    if edit_choice == 2:
        squad_lobby()

    if edit_choice > 2:
        print('there is no such menu, please retry')
        soldier_edit()

def buy_soldier():
    global credit
    global xx,yy
    global sol_list, sol_equipped

    os.system('cls')
    print('----------------- BUY SOLDIER ---------------------')
    print('----------------- CREDIT = %d ---------------------' %credit)

    print('Available soldiers = ', yy)
    print('Soldier equipped = ', xx)
    print('Options:\n(1) BUY\n(2) Back to Squad roster page')
    buy_choice = int(input('Select:'))

    # BUY SOLDIER
    if buy_choice == 1:
        print('Available soldiers:')

        # Print all available soldier lists and its stats
        for list in range(yy):
            print('(%d).' % list)
            print(sol_list[list].soldierStat())

        # Select soldier
        select_soldier = int(input('SELECT: '))
        if (select_soldier > yy):
            print('There is no such soldier, please reply')
            buy_soldier()

        # If the player has enough credit to buy the soldier
        if credit >= sol_list[select_soldier].cost:
            print('Are you sure to unlock soldier %s?' % sol_list[select_soldier].name)
            print('(1) YES (2) NO')
            decision = int(input('Select:'))

            # YES I will buy this soldier
            if decision == 1:
                credit = credit - sol_list[select_soldier].cost
                print('Soldier %s UNLOCKED!' % sol_list[select_soldier].name)
                yy = yy - 1
                xx = xx + 1
                sol_equipped.insert(0, sol_list[select_soldier])
                sol_list.remove(sol_list[select_soldier])
                buy_soldier()

            # Ermm after thinking it for a while, nope
            elif decision == 2:
                buy_soldier()
        
        # The player doesn't have enough credit to buy the soldier
        else:
            print('Sorry you do not have enough credit')
            buy_soldier()

    # Go back to soldier edit page
    elif buy_choice == 2:
        soldier_edit()

    elif buy_choice > 2:
        print('there is no such menu, please retry')
        buy_soldier()

def captain_edit():
    global credit
    global experience
    os.system('cls')

    print('--------------------------------------------------------------')
    print('----------------------- EDIT CAPTAIN -------------------------')
    print('--------------------------------------------------------------')
    print('\n-------------------- CREDIT = %d -----------------------------' % credit)
    print('--------------------- EXPERIENCE = %d ------------------------' % experience)

    print('\nCAPTAIN STAT:')
    givenCaptain.captainStat()

    print('Options:\n(1) Captain Skills and Items\n(2) Back to squad lobby')
    cap_choice = int(input('Select:'))

    if cap_choice == 1:
        print('Not yet available in this alpha prototype')
        captain_edit()

    # Back to squad lobby
    elif cap_choice == 2:
        squad_lobby()

    elif cap_choice > 2:
        print('there is no such a menu, please retry')
        captain_edit()

def hierophant_edit():
    global credit
    global experience
    os.system('cls')

    print('--------------------------------------------------------------')
    print('---------------------- EDIT HIEROPHANT -----------------------')
    print('--------------------------------------------------------------')
    print('\n-------------------- CREDIT = %d -----------------------------' % credit)
    print('--------------------- EXPERIENCE = %d ------------------------' % experience)

    print('\nHIEROPHANT STAT:')
    givenHierophant.hierophantStat()

    print('Options:\n(1) Hierophant Skills and Items\n(2) Back to squad lobby')
    hie_choice = int(input('Select:'))

    if hie_choice == 1:
        print('Not yet available in this alpha prototype')
        hierophant_edit()

    # Back to squad lobby
    elif hie_choice == 2:
        squad_lobby()

    elif hie_choice > 2:
        print('there is no such a menu, please retry')
        hierophant_edit()

#================================================================================================
squad_lobby()